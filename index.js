
//Without Event Deligation
document.getElementById("box1").addEventListener('click', (e) => {
    let target = e.target
    target.innerText = 1
    target.style.display = "flex"
    target.style.alignItems = "center"
    target.style.justifyContent = "center"
    target.style.color = "white"
    target.style.fontSize = "64px"
    setTimeout(() => {
        target.innerText = ""
    }, 5000)
})

document.getElementById("box2").addEventListener('click', (e) => {
    let target = e.target
    target.innerText = 2
    target.style.display = "flex"
    target.style.alignItems = "center"
    target.style.justifyContent = "center"
    target.style.color = "white"
    target.style.fontSize = "64px"
    setTimeout(() => {
        target.innerText = ""
    }, 5000)
})

document.getElementById("box3").addEventListener('click', (e) => {
    let target = e.target
    target.innerText = 3
    target.style.display = "flex"
    target.style.alignItems = "center"
    target.style.justifyContent = "center"
    target.style.color = "white"
    target.style.fontSize = "64px"
    setTimeout(() => {
        target.innerText = ""
    }, 5000)
})

document.getElementById("box4").addEventListener('click', (e) => {
    let target = e.target
    target.innerText = 4
    target.style.display = "flex"
    target.style.alignItems = "center"
    target.style.justifyContent = "center"
    target.style.color = "white"
    target.style.fontSize = "64px"
    setTimeout(() => {
        target.innerText = ""
    }, 5000)
})

document.getElementById("box5").addEventListener('click', (e) => {
    let target = e.target
    target.innerText = 5
    target.style.display = "flex"
    target.style.alignItems = "center"
    target.style.justifyContent = "center"
    target.style.color = "white"
    target.style.fontSize = "64px"
    setTimeout(() => {
        target.innerText = ""
    }, 5000)
})

document.getElementById("box6").addEventListener('click', (e) => {
    let target = e.target
    target.innerText = 6
    target.style.display = "flex"
    target.style.alignItems = "center"
    target.style.justifyContent = "center"
    target.style.color = "white"
    target.style.fontSize = "64px"
    setTimeout(() => {
        target.innerText = ""
    }, 5000)
})

document.getElementById("box7").addEventListener('click', (e) => {
    let target = e.target
    target.innerText = 7
    target.style.display = "flex"
    target.style.alignItems = "center"
    target.style.justifyContent = "center"
    target.style.color = "white"
    target.style.fontSize = "64px"
    setTimeout(() => {
        target.innerText = ""
    }, 5000)
})

document.getElementById("box8").addEventListener('click', (e) => {
    let target = e.target
    target.innerText = 8
    target.style.display = "flex"
    target.style.alignItems = "center"
    target.style.justifyContent = "center"
    target.style.color = "white"
    target.style.fontSize = "64px"
    setTimeout(() => {
        target.innerText = ""
    }, 5000)
})

document.getElementById("box9").addEventListener('click', (e) => {
    let target = e.target
    target.innerText = 9
    target.style.display = "flex"
    target.style.alignItems = "center"
    target.style.justifyContent = "center"
    target.style.color = "white"
    target.style.fontSize = "64px"
    setTimeout(() => {
        target.innerText = ""
    }, 5000)
})

document.getElementById("box10").addEventListener('click', (e) => {
    let target = e.target
    target.innerText = 10
    target.style.display = "flex"
    target.style.alignItems = "center"
    target.style.justifyContent = "center"
    target.style.color = "white"
    target.style.fontSize = "64px"
    setTimeout(() => {
        target.innerText = ""
    }, 5000)
})

document.getElementById("box11").addEventListener('click', (e) => {
    let target = e.target
    target.innerText = 11
    target.style.display = "flex"
    target.style.alignItems = "center"
    target.style.justifyContent = "center"
    target.style.color = "white"
    target.style.fontSize = "64px"
    setTimeout(() => {
        target.innerText = ""
    }, 5000)
})

document.getElementById("box12").addEventListener('click', (e) => {
    let target = e.target
    target.innerText = 12
    target.style.display = "flex"
    target.style.alignItems = "center"
    target.style.justifyContent = "center"
    target.style.color = "white"
    target.style.fontSize = "64px"
    setTimeout(() => {
        target.innerText = ""
    }, 5000)
})

//with Event Deligation

document.querySelector(".container2").addEventListener('click', (e) => {
    let target = e.target
    if (target.matches('div')) {
        target.style.display = "flex"
        target.style.alignItems = "center"
        target.style.justifyContent = "center"
        target.style.color = "white"
        target.style.fontSize = "64px"
        target.innerText = target.getAttribute('num')
        setTimeout(() => {
            target.innerText = ""
        }, 5000)
    }
})

